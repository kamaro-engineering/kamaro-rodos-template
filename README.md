# kamaro-rodos-template

Template for RODOS projects used at Kamaro-Engineering e.V.

## Fetch Sources

```shell script
$ mkdir external
$ vcs import external < repos.txt
```

## Update Sources

```shell script
$ vcs pull external
```

## Build

```shell script
$ mkdir build
$ cd build
$ cmake -DCMAKE_TOOLCHAIN_FILE=../external/rodos/cmake/port/linux-makecontext.cmake -DEXECUTABLE=ON ..
$ make all
```

## Project Layout / Design
See [The Pitchfork Layout PFL](https://api.csswg.org/bikeshed/?force=1&url=https://raw.githubusercontent.com/vector-of-bool/pitchfork/develop/data/spec.bs)

The dependency hierarchy is:
* RODOS
* Messages
* Topics
* Other nodes, like kamaro-rodos-beacon
* The project
