#!/bin/sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

rm -rf build
mkdir build
cd build

cmake -DCMAKE_TOOLCHAIN_FILE=../external/rodos/cmake/port/linux-makecontext.cmake -DEXECUTABLE=ON ..
make all
